# Pairs

## Input test data
Add Data to input.txt

## Sample Data:
```
Parametr 1: 6, 10, 15, 30, 60

Parametr 2: 98, NT, 2000, XP

Parametr 3: Internal, Modem

Parametr 4: Salaried, Hourly, Part-Time, Contr.

Parametr 5: Brand X, Brand Y
```

## Docker run:
```
start.sh
```

## Non docker run:
```
nondocker.sh
```

## Result:
Check result in output/ folder
### Result output.csv
```
Case, Parametr1, Parametr2, Parametr3, Parametr4, Parametr5, 
1, 6, 98, Internal, Salaried, Brand X,
2, 10, NT, Modem, Hourly, Brand X,
3, 15, 2000, Modem, Part-Time, Brand Y,
4, 30, XP, Internal, Contr., Brand Y,
5, 60, XP, Modem, Salaried, Brand Y,
6, 60, 2000, Internal, Hourly, Brand X,
7, 30, NT, Internal, Part-Time, Brand X,
8, 15, 98, Modem, Contr., Brand X,
9, 10, 98, Internal, Part-Time, Brand Y,
10, 6, XP, Modem, Hourly, Brand Y,
11, 15, NT, Internal, Salaried, Brand Y,
12, 30, 2000, Modem, Salaried, Brand Y,
13, 6, 2000, Modem, Contr., Brand Y,
14, 10, XP, Modem, Salaried, Brand X,
15, 60, NT, Modem, Contr., Brand Y,
16, 60, 98, Modem, Part-Time, Brand Y,
17, 10, 2000, Modem, Contr., Brand Y,
18, 6, NT, Modem, Part-Time, Brand Y,
19, 30, 98, Modem, Hourly, Brand Y,
20, 15, XP, Modem, Part-Time, Brand Y,
21, 15, XP, Modem, Hourly, Brand Y,
 ```

 ## Sample Data and filters:
  If the line starts with "#IF" it is perceived as a filter. If the string starts otherwise it is not a filter. Each parameter and value is written in double quotes ".
  If the string starts with "#" the string is not test data and is ignored.
The order of filling the parameter and filters does not matter.
### input.txt
 ```
Parametr 1: 6, 10, 15, 30, 60

Parametr 2: 98, NT, 2000, XP

Parametr 3: Internal, Modem

Parametr 4: Salaried, Hourly, Part-Time, Contr.

Parametr 5: Brand X, Brand Y

#IF "Parametr 1" = "15" THEN "Parametr 2" = "NT"
#IF"Parametr 3"!="Internal"THEN"Parametr 4"="Hourly"
## Same text
```
### output.csv
```
Case, Parametr1, Parametr2, Parametr3, Parametr4, Parametr5, 
1, 6, 98, Internal, Salaried, Brand X,
2, 10, NT, Modem, Hourly, Brand X,
3, 15, NT, Internal, Part-Time, Brand Y,
4, 30, 2000, Modem, Hourly, Brand Y,
5, 60, XP, Modem, Hourly, Brand Y,
6, 60, 2000, Internal, Contr., Brand X,
7, 30, 98, Modem, Hourly, Brand X,
8, 15, NT, Modem, Hourly, Brand X,
9, 10, XP, Internal, Contr., Brand Y,
10, 6, XP, Modem, Hourly, Brand X,
11, 30, XP, Internal, Part-Time, Brand X,
12, 6, NT, Modem, Hourly, Brand Y,
```

