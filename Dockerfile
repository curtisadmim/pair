FROM python:3

COPY src/ /usr/app/src/
COPY input.txt /usr/app/src/

WORKDIR /usr/app/src

VOLUME /usr/app/output

RUN pip --no-cache-dir install --upgrade -r requirements.txt

ENTRYPOINT ["python", "main.py"]