import pathlib
from source import *
import logging
import logging.config


def main():
    pathlib.Path('./output').mkdir(parents=True, exist_ok=True)
    log_file_path = pathlib.Path("./output/result.log")
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logging.FileHandler(log_file_path),
            logging.StreamHandler()
        ])
    logger = logging.getLogger()
    logger.info("Program started")
    dh = DataHelper
    sour = dh.indata(logger)
    if sour is None:
        logger.info("End task")
        return
    logger.info("Write data")
    dh.outdata(dh, logger, sour)


if __name__ == '__main__':
    main()
