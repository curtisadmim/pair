import pathlib
from collections import OrderedDict
from allpairspy import AllPairs
import logging
import logging.config


class DataHelper:
    array = []
    def indata(logger):
        logger.info("Read data")
        d = {}
        try:
            file = open("input.txt")
            for line in file:
                line.split()
                if len(line)>1 and line[:1] != "#":
                    param = line.strip().split(":")[0].replace(' ', '')
                    data = line.strip().split(":")[1].split(",")
                    d[param] = [x.strip() for x in data]
        except Exception as e:
            logger.critical("File read error: " + str(e))
            return None
        logger.info("Complete read data:")
        logger.info(d)
        return OrderedDict(d)

    def outdata(self, logger, sour):
        rez = enumerate(AllPairs(sour, filter_func=self.is_valid_combination))
        head = "Case, "
        for s in sour.keys():
            head = head + list([s])[0] + ", "
        try:
            pathlib.Path('./output').mkdir(parents=True, exist_ok=True)
            file = open("./output/output.csv", 'w')
            file.write(head + '\n')
            logger.debug(head)
            for i, pairs in rez:
                result = ""
                for st in pairs:
                    result = result + " " + st + ","
                file.write(str(i+1) + "," + result + '\n')
                logger.debug(str(i+1) + "," + result)
        except Exception as e:
            logger.critical(e)
            return

    def is_valid_combination(row):
        df = DataFinder
        array = df.getfilters(df)
        if len(array) < 1:
            return True
        for req in array:
            bools = True
            param1 = req[1].replace(' ', '')
            param2 = req[5].replace(' ', '')
            value1 = req[3]
            value2 = req[7]
            comp1 = req[2].strip()
            comp2 = req[6].strip()
            if comp1.strip() == "=":
                bools = df.get_equals(df, param1, param2, value1, value2, comp2, row)
            if bools is False:
                return bools
            if comp1.strip() == "!=" or comp1.strip() == "=!" or comp1.strip() == "<>" or comp1.strip() == "><":
                df.logger.info("param check")
                bools = df.get_notequals(df, param1, param2, value1, value2, comp2, row)
            if bools is False:
                return bools
            if comp1.strip() == ">" or comp1.strip() == "<":
                df.logger.info("Comparison " + comp1 + " not supported")
        return bools


class DataFinder:
    pathlib.Path('./output').mkdir(parents=True, exist_ok=True)
    log_file_path = pathlib.Path("./output/result.log")
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logging.FileHandler(log_file_path),
            logging.StreamHandler()
        ])
    logger = logging.getLogger()


    def getfilters(self):
        list = []
        try:
            file = open("input.txt")
            for line in file:
                line.split()
                if line[:3] == "#IF":
                    list.append(line.split('"'))
        except Exception as e:
            self.logger.critical(e)
            return None
        return list

    def get_param(self):
        list = []
        try:
            file = open("input.txt")
            for line in file:
                line.split()
                if len(line)>1 and line[:3] != "#IF":
                    param = line.strip().split(":")[0].replace(' ', '')
                    list.append(param)
        except Exception as e:
            self.logger.critical("File read error: " + str(e))
            return None
        return list




    def get_equals(self, param1, param2, value1, value2, comp2, row):
        param = self.get_param(self)
        index1 = self.get_index(self,param,param1)
        index2 = self.get_index(self,param,param2)
        if len(row) > index1 and len(row)>index2:
            if row[index1] == value1:
                return self.get_action(self,row[index2],value2,comp2)
        return True

    def get_notequals(self, param1, param2, value1, value2, comp2, row):
        param = self.get_param(self)
        index1 = self.get_index(self,param,param1)
        index2 = self.get_index(self,param,param2)
        if len(row) > index1 and len(row)>index2:
            if row[index1] != value1:
                return self.get_action(self,row[index2],value2,comp2)
        return True


    def get_action(self,row,value2,comp2):
        if comp2 == '=':
            if row != value2:
                self.logger.info("param is ready")
                return False
            return True
        if comp2 == '!=':
            self.logger.info("param is ready")
            if row == value2:
                return False
            return True
        if comp2 == '=!' or comp2 == '<>':
            if row == value2:
                self.logger.info("param is ready")
                return False
            return True
        if comp2 == '>' or comp2 == '<':
            self.logger.info("param not support")
            return True




    def get_index(self,param,param1):
        p1 = None
        index = 0
        for i in param:
            if i == param1:
                return index
            index = index +1
        self.logger.critical("Parametr Not Found. Filters not works")
        return None